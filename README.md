# request_event_tools

Request_event_tools is a tool to retrieve a list of documents based on a given event. The tool is based on Elasticsearch. It can do different types of queries.

## Request
| QUERIES | Entities named event | Title event | Entities named documents | Title and description documents | Weighted boost | Normalized boost | Fuzziness
|------------|------------|------------|------------|------------|------------|------------|------------|
| Query 1 | [x] | [ ] | [ ] | [x] | [x] | [ ] | [x]
| Query 2 | [ ] | [x] | [ ] | [x] | [ ] | [ ] | [ ]
| Query 3 | [x] | [ ] | [x] | [ ] | [x] | [ ] | [x]
| Query 4 | [x] | [ ] | [ ] | [x] | [ ] | [x] | [x]
| Query 5 | [x] | [ ] | [x] | [ ] | [ ] | [x] | [x]
| Query 6 | [ ] | [x] | [ ] | [x] | [ ] | [ ] | [x]
| Query 7 | [x] | [ ] | [ ] | [x] | [ ] | [x] | [ ]
| Query 8 | [x] | [x] | [ ] | [x] | [ ] | [x] | [x]
| Query 9 | [x] | [ ] | [ ] | [x] | [ ] | [x] | [ ]
| Query 10 | [x] | [x] | [ ] | [x] | [ ] | [x] | [x]
| Query 11 | [x] | [x] | [ ] | [x] | [x] | [ ] | [ ]
| Query 12  | [x] | [x] | [ ] | [x] | [x] | [ ] | [x]

## Note :
Request 9, 10 and 11 have specificities.
* Query 9 has an end date to filter out documents with a later publication date. 
* Query 10 and 11 is dynamic and varies according to the number of named entities. If it has few named entities then it will also use the title of the event.

## requirement
You need to put in the directory the annotated_event.csv.

# Experiments
## First experiment

It was necessary to have a first point of view to understand and compare our
results. For this, two queries were set up. The first one
searches for the named entities of an event in the text and the
description of the events.

This query revealed two potential problems.
Firstly, there was a lack of temporal information. In the named
entities, there is no temporal information such as a day or a
year. This can make it difficult to differentiate between two
redundant events (which occur regularly). Another problem is the
problem is the disparity in the quality of the events. Indeed, one
event may have more or fewer named entities than another. An
event may also be poorly described, for example it is possible that an event
An event can also be poorly described, for example it is possible that an event does not have a start or a description field.

## Query with event title

This experiment attempts to solve problems seen in
experiment 1. First of all the lack of temporal information. In
In this query, it is not the named entities of the event, but its
but its title (broken down word by word) which are searched in the title and the
description of the documents. It can be seen that the title of the events is
generally quite well supplied. It contains information such as
date, place or even the name of the event (e.g. Sarajevo bombing).

This experiment gave us rather bad results. This can be explained in several ways.
This can be explained in several ways. First of all, a title is
short, so it is possible that the lack of information
information is involved. Finally, it is possible that some
events may not have a title due to their poor quality.

## Query using the named entities of the event

This experiment aims to compare the named entities of events with those of documents.
those of the documents. The results obtained from this experiment
are very poor. An event has several named entities, but
it will retrieve all documents with these named entities, even if there is only one
even if there is only one place or one person.

## Conclusion

Following these experiments we can have a first overview of our approaches and
approaches and possible problems. The next step will be
be to test the parameters one by one to see if it is really relevant to use them.
relevant to use them. Moreover, by calculating the correlation on our
experiments we can see that requests 1 and 3 are weakly correlated, while request
correlated, while the query is not correlated at all. Well
Although the results of the query are the best, we can also observe that
Although the results of the query are the best, we can also observe that the query obtains better results on the
poor quality events.

We also tried to combine queries 1 and 2, but the results were not as good as the
results did not have the desired effect.

## Parameters to test

### The weighting

It is important to check that the weighting ("boost") we are using is
the weighting ("boost") we use is relevant. Currently the more times a named entity has been
times in the text the higher its weight will be. This can
This can cause imbalances, and put too much emphasis on one person and
person and open up our search field. To test our
weighting, we will set up a normalized weight between the different types of
different types of named entities. For example, it is normal for a
For example, it is normal for a place to be more important than a person. Named entities of type
GPE and type ORG will have a weight of two, while named entities of type
named entities of type PER will have a weight of one.

To test the different approaches, two similar queries were implemented.
implemented. They are identical except for the boost.

### Fuzzy queries

Fuzzy queries are very useful in theory. Indeed, we
have OCR problems that can degrade our character recognition.
character recognition. For example, it is possible to recognize the field
Wahsongton instead of Washington. However, it is possible that
these fuzzy searches may include more false results (a well-written term
not matching the search, but with the help of the fuzzy search
retrieved anyway) than good results. Here again, the solution is
to test two identical queries with the exception of fuzziness.

### Conclusion

We can see that weighting does not bring better results.
This can be quite strange, indeed having a badly proportioned boost
can bring an imbalance on our results. It may be relevant to
It may be relevant to do other tests playing on this parameter. As for fuzzy queries,
we can see that the results are better, and therefore that this
parameter currently brings more errors than good results. Here
also, it can be useful to make more tests by reducing for example the
the value of the boost.

## Improvement path

### End date

By looking more precisely at our results we can see that
that some documents that are not related to the event, are in fact related to a
event, are in fact related to a very close event, but at a later date.
To solve this problem, we set an end date so that all documents with a date
documents with a later date are not retrieved.
recovered. To have an end date, it was decided to take one year
after the start date, in order to avoid annual events.
The results were quite bad, as it is possible that
events that took place, for example, 10 years ago may be relevant again.
again. It may be complicated to use this setting without
without losing documents.

### Dynamic queries

We have seen that query 2 allows us to obtain more interesting results on
We have seen that query 2 allows us to obtain more interesting results on events of poor quality. We will use this observation to our advantage.
use this observation to our advantage. For this experiment we will
check the number of named entities. If it is less than a
value then we will use query 2 as support.

We can see that the results are better than queries 1 and
2, as well as the results of merging the two.

### Conclusion

Once we had tested our different parameters, we could see that
some were more useful than others. For example, the
For example, fuzzy queries can be removed, as well as the end date. On the other hand, it is
important to keep a non-normalized boost.

## Optimised Query

Now that we have analysed our results, we know which query is the best and which parameters are the most important.
the best query and which parameters are the most useful. It
to combine all this to get our optimised query.
optimised query.

The results are the best we have been able to obtain so far.
so far.

