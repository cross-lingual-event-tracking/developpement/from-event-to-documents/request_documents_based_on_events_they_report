#!/usr/bin/env python3
#
# Copyright (C) 2022 Guillaume Bernard <contact@guillaume-bernard.fr>,
# Copyright (C) 2022 Thomas Blot <thomas.blot@etudiant.univ-lr.fr>,
#
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
from ast import If
from datetime import datetime, timedelta
import json
import os
import elasticsearch
import pandas as pd
import urllib3
from wikivents.factories import ProgressBarHandler
from wikivents.factories.wikimedia import WikimediaFactory
from wikivents.model_encoders import EventToDictEncoder
from wikivents.models import EntityId, ISO6391LanguageCode, Event
from typing import List, Dict, Set
from elasticsearch import Elasticsearch
from time import process_time

# We do disable certificate verification, so it’s clearer to disable warnings
urllib3.disable_warnings()


class ElasticSearch:
    def __init__(self, host: str, password: str, port: int, user: str):
        self.__connection = Elasticsearch(
            f"https://{host}:{port}", basic_auth=(user, password), verify_certs=False
        )

    def query_execute(self, query: dict, index: str, size: int):
        return self.__connection.search(index=index, query=query, size=size)

    def get_index(self):
        return self.__connection.indices.get_alias().keys()


def process_a_single_event(entity: EntityId) -> Event:
    return WikimediaFactory.get_instance().create_event(entity, ProgressBarHandler())


def get_the_localised_dict_repr_of_processed_events(
    events_to_localise: Set[Event], iso_639_1_language_code: ISO6391LanguageCode
) -> List[Dict]:
    localised_events = []
    for event in events_to_localise:
        localised_events.append(
            EventToDictEncoder(event, iso_639_1_language_code).encode()
        )
    return localised_events


def retrieve_documents_related_to_event(
    db: ElasticSearch,
    event: dict,
    operator: str,
    days: int,
    index: str,
    size: int,
    lang: str,
    type_query: str,
) -> dict:
    event["start"] = increase_range_of_date_by_X_days(event["start"], days)
    query = query_builder(event, operator, lang, type_query)
    documents = db.query_execute(query, index, size)
    return documents, query


def query_export(query: dict, save_to: str):
    df = pd.DataFrame(query)
    df.to_json(save_to, indent=2)


def query_builder(events: Dict, operator: str, lang: str, type_query: str) -> dict:
    query_function_dict = {
        "1": search_named_entities_of_event_in_title_and_text_of_documents,
        "2": search_title_of_event_in_title_and_text_of_documents_without_fuzziness,
        "3": search_named_entities_of_event_in_named_entities_of_documents,
        "4": search_named_entities_of_event_in_title_and_text_of_documents_with_standardised_weight,
        "5": search_named_entities_of_event_in_named_entities_of_documents_with_standardised_weight,
        "6": search_title_of_event_in_title_and_text_of_documents,
        "7": search_named_entities_of_event_in_title_and_text_of_documents_with_standardised_weight_and_without_fuzziness,
        "8": search_named_entities_of_event_and_title_in_title_and_text_and_named_entities_of_documents_with_standardised_weight_and_without_fuzziness,
        "9": search_named_entities_of_event_in_title_and_text_of_documents_with_standardised_weight_and_without_fuzziness_and_with_end_date,
        "10": search_named_entities_if_exists_else_title,
        "11": search_named_entities_of_event_in_title_and_text_of_documents__without_fuzziness_and_if_the_number_of_entities_is_less_than_5_use_event_name_too,
        "12": search_named_entities_of_event_and_title_in_title_and_text_and_named_entities_of_documents_and_without_fuzziness
    }
    return query_function_dict[type_query](events, operator, lang)


# Query looking for named entities of the event in the title and text of documents
def search_named_entities_of_event_in_title_and_text_of_documents(
    events: Dict, operator: str, lang: str
):
    query = {"bool": {"must": [], "should": []}}
    searching_field = ["title.raw", "title.cleaned", "text.cleaned", "text.raw"]
    for entity, description_of_entity in events.items():
        if entity_is_start_date_and_exist(entity, events):
            query["bool"]["must"].append({"range": {"date": {"gte": events["start"]}}})
        elif entity_is_a_named_entity(entity):
            query["bool"][operator].append(
                {
                    "multi_match": {
                        "query": entity,
                        "fuzziness": "AUTO",
                        "fields": searching_field,
                        "boost": description_of_entity[0],
                    }
                }
            )
    query["bool"]["must"].append({"match": {"language": {"query": lang}}})
    return query


# Query looking for the title of the event in the title and text of documents without using fuzziness
def search_title_of_event_in_title_and_text_of_documents_without_fuzziness(
    events: Dict, operator: str, lang: str
) -> dict:
    query = {"bool": {"must": [], "should": []}}
    searching_field = ["title.raw", "title.cleaned", "text.cleaned", "text.raw"]
    for entity, description_of_entity in events.items():
        if entity_is_start_date_and_exist(entity, events):
            query["bool"]["must"].append({"range": {"date": {"gte": events["start"]}}})
        elif entity_is_a_named_entity(entity):
            for field in searching_field:
                query["bool"]["should"].append(
                    {
                        "match": {
                            field: {
                                "query": _remove_ocr_markup_from_raw_text(
                                    events["label"]
                                )
                            }
                        }
                    }
                )
    query["bool"]["must"].append({"match": {"language": {"query": lang}}})
    return query


# Query looking for named entities of the event in the named_entities of documents
def search_named_entities_of_event_in_named_entities_of_documents(
    events: Dict, operator: str, lang: str
) -> dict:
    query = {"bool": {"must": [], "should": []}}
    searching_field = ["mentions.label"]
    for entity, description_of_entity in events.items():
        if entity == "start" and events["start"]:
            query["bool"]["must"].append({"range": {"date": {"gte": events["start"]}}})
        elif entity_is_a_named_entity(entity):
            query["bool"][operator].append(
                {
                    "multi_match": {
                        "query": entity,
                        "fuzziness": "AUTO",
                        "fields": searching_field,
                        "boost": description_of_entity[0],
                    }
                }
            )
    query["bool"]["must"].append({"match": {"language": {"query": lang}}})
    return query


# Query looking for named entities of the event in the title and text of
# the documents using a weighting according to the type of the named entity
def search_named_entities_of_event_in_title_and_text_of_documents_with_standardised_weight(
    events: Dict, operator: str, lang: str
):
    query = {"bool": {"must": [], "should": []}}
    searching_field = ["title.raw", "title.cleaned", "text.cleaned", "text.raw"]
    for entity, description_of_entity in events.items():
        if entity_is_start_date_and_exist(entity, events):
            query["bool"]["must"].append({"range": {"date": {"gte": events["start"]}}})
        elif entity_is_a_named_entity(entity):
            if named_entity_is_a_person(description_of_entity[1]):
                boost = 1
            else:
                boost = 2
            query["bool"][operator].append(
                {
                    "multi_match": {
                        "query": entity,
                        "fuzziness": "AUTO",
                        "fields": searching_field,
                        "boost": boost,
                    }
                }
            )
    query["bool"]["must"].append({"match": {"language": {"query": lang}}})
    return query


# Query looking for named entities of the event in the named entities of
# the documents using a weighting according to the type of the named entity
def search_named_entities_of_event_in_named_entities_of_documents_with_standardised_weight(
    event: Dict, operator: str, lang: str
) -> dict:
    query = {"bool": {"must": [], "should": []}}
    searching_field = ["mentions.label"]
    for entity, description_of_entity in event.items():
        if entity == "start" and event["start"]:
            query["bool"]["must"].append({"range": {"date": {"gte": event["start"]}}})
        elif entity_is_a_named_entity(entity):
            if named_entity_is_a_person(description_of_entity[1]):
                boost = 1
            else:
                boost = 2
            query["bool"][operator].append(
                {
                    "multi_match": {
                        "query": entity,
                        "fuzziness": "AUTO",
                        "fields": searching_field,
                        "boost": boost,
                    }
                }
            )
    query["bool"]["must"].append({"match": {"language": {"query": lang}}})
    return query


# Query looking for the title of the event in the title and text of documents using fuzziness
def search_title_of_event_in_title_and_text_of_documents(
    events: Dict, operator: str, lang: str
) -> dict:
    query = {"bool": {"must": [], "should": []}}
    searching_field = ["title.raw", "title.cleaned", "text.cleaned", "text.raw"]
    for entity, description_of_entity in events.items():
        if entity_is_start_date_and_exist(entity, events):
            query["bool"]["must"].append({"range": {"date": {"gte": events["start"]}}})
        elif entity_is_a_named_entity(entity):
            for field in searching_field:
                query["bool"]["should"].append(
                    {
                        "match": {
                            field: {
                                "query": _remove_ocr_markup_from_raw_text(
                                    events["label"]
                                ),
                                "fuzziness": "AUTO",
                            }
                        }
                    }
                )
    query["bool"]["must"].append({"match": {"language": {"query": lang}}})
    return query


# Query looking for named entities of the event in the title and text of
# the documents using a weighting according to the type of the named entity without fuzziness
def search_named_entities_of_event_in_title_and_text_of_documents_with_standardised_weight_and_without_fuzziness(
    events: Dict, operator: str, lang: str
):
    query = {"bool": {"must": [], "should": []}}
    searching_field = ["title.raw", "title.cleaned", "text.cleaned", "text.raw"]
    for entity, description_of_entity in events.items():
        if entity_is_start_date_and_exist(entity, events):
            query["bool"]["must"].append({"range": {"date": {"gte": events["start"]}}})
        elif entity_is_a_named_entity(entity):
            if named_entity_is_a_person(description_of_entity[1]):
                boost = 1
            else:
                boost = 2
            query["bool"][operator].append(
                {
                    "multi_match": {
                        "query": entity,
                        "fields": searching_field,
                        "boost": boost,
                    }
                }
            )
    query["bool"]["must"].append({"match": {"language": {"query": lang}}})
    return query


# Query looking for named entities and title of the event in the title and text of
# the documents using a weighting according to the type of the named entity without fuzziness
def search_named_entities_of_event_and_title_in_title_and_text_and_named_entities_of_documents_with_standardised_weight_and_without_fuzziness(
    events: Dict, operator: str, lang: str
):
    query = {"bool": {"must": [], "should": []}}
    searching_field = ["title.raw", "title.cleaned", "text.cleaned", "text.raw"]
    for entity, description_of_entity in events.items():
        if entity_is_start_date_and_exist(entity, events):
            query["bool"]["must"].append({"range": {"date": {"gte": events["start"]}}})
        elif entity_is_a_named_entity(entity):
            if named_entity_is_a_person(description_of_entity[1]):
                boost = 1
            else:
                boost = 2
            query["bool"][operator].append(
                {
                    "multi_match": {
                        "query": entity,
                        "fields": searching_field,
                        "boost": boost,
                    }
                }
            )
            query["bool"][operator].append(
                {
                    "multi_match": {
                        "query": _remove_ocr_markup_from_raw_text(events["label"]),
                        "fields": searching_field,
                        "boost": 3,
                    }
                }
            )
    query["bool"]["must"].append({"match": {"language": {"query": lang}}})
    return query


# Query looking for named entities of the event in the title and text
# of the documents using a weighting according to the type
# of the named entity without fuzziness and with end date.
def search_named_entities_of_event_in_title_and_text_of_documents_with_standardised_weight_and_without_fuzziness_and_with_end_date(
    events: Dict, operator: str, lang: str
):
    query = {"bool": {"must": [], "should": []}}
    searching_field = ["title.raw", "title.cleaned", "text.cleaned", "text.raw"]
    for entity, description_of_entity in events.items():
        if entity_is_start_date_and_exist(entity, events):
            end_date = (
                events["start"][:3]
                + str(int(events["start"][3]) + 1)
                + events["start"][3 + 1 :]
            )
            query["bool"]["must"].append(
                {"range": {"date": {"gte": events["start"], "lte": end_date}}}
            )
        elif entity_is_a_named_entity(entity):
            if named_entity_is_a_person(description_of_entity[1]):
                boost = 1
            else:
                boost = 2
            query["bool"][operator].append(
                {
                    "multi_match": {
                        "query": entity,
                        "fields": searching_field,
                        "boost": boost,
                    }
                }
            )
    query["bool"]["must"].append({"match": {"language": {"query": lang}}})
    return query


# Query using the named entities of the event and searching in the titles
# and descriptions of the documents. If the number of named entities is less than 5 then
# it will also use the event title.
def search_named_entities_if_exists_else_title(events: Dict, operator: str, lang: str):
    query = {"bool": {"must": [], "should": []}}
    searching_field = ["title.raw", "title.cleaned", "text.cleaned", "text.raw"]
    for entity, description_of_entity in events.items():
        if entity_is_start_date_and_exist(entity, events):
            query["bool"]["must"].append({"range": {"date": {"gte": events["start"]}}})
        elif entity_is_a_named_entity(entity):
            if named_entity_is_a_person(description_of_entity[1]):
                boost = 1
            else:
                boost = 2
            query["bool"][operator].append(
                {
                    "multi_match": {
                        "query": entity,
                        "fuzziness": "AUTO",
                        "fields": searching_field,
                        "boost": boost,
                    }
                }
            )
        if event_is_badly_described(events):
            query["bool"][operator].append(
                {
                    "multi_match": {
                        "query": _remove_ocr_markup_from_raw_text(events["label"]),
                        "fields": searching_field,
                        "boost": 3,
                    }
                }
            )
    query["bool"]["must"].append({"match": {"language": {"query": lang}}})
    return query


# Query using the named entities of the event and searching in the titles
# and descriptions of the documents. If the number of named entities is less than 5 then
# it will also use the event title. Without fuzziness.
def search_named_entities_of_event_in_title_and_text_of_documents__without_fuzziness_and_if_the_number_of_entities_is_less_than_5_use_event_name_too(
    events: Dict, operator: str, lang: str
):
    query = {"bool": {"must": [], "should": []}}
    searching_field = ["title.raw", "title.cleaned", "text.cleaned", "text.raw"]
    for entity, description_of_entity in events.items():
        if entity_is_start_date_and_exist(entity, events):
            query["bool"]["must"].append({"range": {"date": {"gte": events["start"]}}})
        elif entity_is_a_named_entity(entity):
            query["bool"][operator].append(
                {
                    "multi_match": {
                        "query": entity,
                        "fields": searching_field,
                        "boost": description_of_entity[0],
                    }
                }
            )
        if event_is_badly_described(events):
            query["bool"][operator].append(
                {
                    "multi_match": {
                        "query": _remove_ocr_markup_from_raw_text(events["label"]),
                        "fields": searching_field,
                        "boost": 3,
                    }
                }
            )
    query["bool"]["must"].append({"match": {"language": {"query": lang}}})
    return query

def search_named_entities_of_event_and_title_in_title_and_text_and_named_entities_of_documents_and_without_fuzziness(
    events: Dict, operator: str, lang: str    
):
    query = {"bool": {"must": [], "should": []}}
    searching_field = ["title.raw", "title.cleaned", "text.cleaned", "text.raw"]
    for entity, description_of_entity in events.items():
        if entity_is_start_date_and_exist(entity, events):
            query["bool"]["must"].append({"range": {"date": {"gte": events["start"]}}})
        elif entity_is_a_named_entity(entity):
            query["bool"][operator].append(
                {
                    "multi_match": {
                        "query": entity,
                        "fields": searching_field,
                        "boost":  description_of_entity[0],
                    }
                }
            )
            query["bool"][operator].append(
                {
                    "multi_match": {
                        "query": _remove_ocr_markup_from_raw_text(events["label"]),
                        "fields": searching_field,
                        "boost": 3,
                    }
                }
            )
    query["bool"]["must"].append({"match": {"language": {"query": lang}}})
    return query


def get_entity_from_event(event: list, event_id: str) -> dict:
    entities = {}
    for key, named_entity in event[0][event_id]["entities"].items():
        for entity in named_entity:
            entities[_remove_ocr_markup_from_raw_text(entity["label"])] = [
                entity["count"],
                entity["type"],
            ]
    entities["start"] = event[0][event_id]["start"]
    entities["label"] = event[0][event_id]["label"]
    return entities


def entity_is_a_named_entity(entity) -> bool:
    return entity != "start" and entity != "label"


def event_is_badly_described(event: dict) -> bool:
    return len(event.items()) < 5


def entity_is_start_date_and_exist(entity, events: dict) -> bool:
    return entity == "start" and events["start"]


def named_entity_is_a_person(entity) -> bool:
    return "PERSON" in entity


def increase_range_of_date_by_X_days(date: str, days: int) -> str:
    new_date = ""
    if date:
        date_in_object = datetime.strptime(date, "%Y-%m-%dT%H:%M:%S%z")
        new_date = date_in_object - timedelta(days=days)
        new_date = new_date.isoformat()
    return new_date


def seperate_data_into_list(documents: dict) -> dict:
    documents_id = []
    documents_score = []
    documents_cluster = []
    for document_id, document_data in documents.items():
        documents_id.append(document_id)
        documents_score.append(document_data[0])
        documents_cluster.append(document_data[1])
    data = {
        "document_id": documents_id,
        "document_score": documents_score,
        "document_cluster": documents_cluster,
    }
    return data


def write_into_file(
    documents: dict, instance_id: str, lang: str, folder: str, index: str
):
    data = seperate_data_into_list(documents)
    df = pd.DataFrame(data)
    df.to_csv(f"{folder}/{index}/{instance_id}/results_{lang}.csv", index=False)


def create_folder(save_to: str, index: str):
    if not os.path.exists(f"{save_to}"):
        os.makedirs(f"{save_to}")
    if not os.path.exists(f"{save_to}/{index}"):
        os.makedirs(f"{save_to}/{index}")


def create_folder_event(save_to: str, index: str, instance_id: str):
    if not os.path.exists(f"{save_to}"):
        os.makedirs(f"{save_to}")
    if not os.path.exists(f"{save_to}/{index}"):
        os.makedirs(f"{save_to}/{index}")
    if not os.path.exists(f"{save_to}/{index}/{instance_id}"):
        os.makedirs(f"{save_to}/{index}/{instance_id}")


def export_event(events: dict, instance_id: str, save_to: str, index: str, lang: str):
    create_folder_event(save_to, index, instance_id)
    with open(f"{save_to}/{index}/{instance_id}/event_{lang}.json", mode="w") as file:
        json.dump(events, file, indent=2)


def _remove_ocr_markup_from_raw_text(input_text: str):
    """
    The noise consists of characters recognized by the OCR system. The size of the string should
    not change in length, as a lot of things are queried from the raw text, or the cleaned one.
    """
    input_text = (input_text.encode("ascii", "replace")).decode("utf-8")
    input_text = input_text.replace("?", " ")
    return (
        input_text.replace("-", " ")  # remove markup for some compound words
        .replace("\n", " ")  # remove newline
        .replace("\t", " ")  # remove tabs
    )


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--instance-id",
        type=str,
        required=True,
        help="The Wikidata instance identifier of one specific entity. For instance, in order to "
        "export the “Easter Rising” event, write “Q193689”.",
    )

    # Language dependent arguments
    parser.add_argument(
        "-l",
        "--language",
        type=ISO6391LanguageCode,
        required=False,
        default=ISO6391LanguageCode("en"),
        help="Language used to export the event.",
    )

    parser.add_argument(
        "--operator",
        required=False,
        type=str,
        help="Operator used to combine the events. Possible values are: must, should, must_not. ",
        default="must",
    )

    parser.add_argument(
        "--host",
        type=str,
        required=False,
        help="ip address on which the server is hosted (default: localhost)",
        default="localhost",
    )
    parser.add_argument(
        "--port",
        type=int,
        required=False,
        help="database listen port (default: 9200)",
        default=9200,
    )
    parser.add_argument(
        "--password",
        type=str,
        required=True,
        help="password for the connection to the database",
    )
    parser.add_argument(
        "--user",
        type=str,
        required=False,
        help="user names for the database",
        default="elastic",
    )
    parser.add_argument(
        "--days",
        type=int,
        required=False,
        help="number of days to look back for the events",
        default=365,
    )
    parser.add_argument(
        "--export-data-folder",
        type=str,
        required=True,
        help="Folder where the date is stored",
        default="data",
    )
    parser.add_argument("--type-query", type=str, required=True, help="type of query")
    parser.add_argument(
        "--size", type=int, required=False, help="number of lines kept", default=10000
    )

    args = parser.parse_args()

    elastic_search_database = ElasticSearch(
        args.host, args.password, args.port, args.user
    )
    all_index = elastic_search_database.get_index()

    if args.instance_id:
        t1_start = process_time()
        processed_event = process_a_single_event(args.instance_id)
        localised_events_dicts = get_the_localised_dict_repr_of_processed_events(
            {processed_event}, args.language[:-1] if args.language != "spa" else "es"
        )
        entity = get_entity_from_event(localised_events_dicts, args.instance_id)
        for index in all_index:
            export_event(
                localised_events_dicts,
                args.instance_id,
                args.export_data_folder,
                index,
                args.language,
            )
            create_folder(args.export_data_folder, index)
            t2_start = process_time()
            documents, query = retrieve_documents_related_to_event(
                elastic_search_database,
                entity.copy(),
                args.operator,
                args.days,
                index,
                args.size,
                args.language,
                args.type_query,
            )
            t2_stop = process_time()
            query_export(
                query,
                f"{args.export_data_folder}/{index}/{args.instance_id}/query_{args.language}.json",
            )
            documents_id_and_score = {}
            for document in documents["hits"]["hits"]:
                documents_id_and_score[str(document["_source"]["document_id"])] = [
                    document["_score"],
                    document["_source"]["cluster"],
                ]
            t1_stop = process_time()
            write_into_file(
                documents_id_and_score,
                args.instance_id,
                args.language,
                args.export_data_folder,
                index,
            )
            time = {
                "request_time_in_sec": t2_stop - t2_start,
                "total_time_in_sec": t1_stop - t1_start,
            }
            df_time = pd.DataFrame(time, index=[0])
            df_time.to_csv(
                f"{args.export_data_folder}/{index}/{args.instance_id}/processed_time_{args.language}.csv",
                index=False,
            )


if __name__ == "__main__":
    main()
