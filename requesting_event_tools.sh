#!/bin/bash

processed_languages="eng spa deu"

# $1 : Annotated file
# $2 : Path where'll be stored data
# $3 : Type of query
# $4 : (optional) If filled then run the experiment for this event otherwise for all events

function event_request()
{
  for lang in $processed_languages
  do
      ./request_documents_based_on_events/request.py --instance-id  "$1" -l "$lang" --password elastic --operator should --days 183 --export-data-folder "$2" --type-query "$3"
  done
}
regex='Q[0-9]{2,}'

if [ -z "$4" ]; then
    while read event; do
      if [[ $event =~ $regex ]]; then
        event_request "${BASH_REMATCH}" "$2" "$3"
      fi
  done < $1
else
  event_request "$4" "$2" "$3"
fi